#!/usr/bin/env python3

import argparse
import re
from colorama import Fore, Style


# (Regex to match, substitution template, error string).
# Use \<n> to insert nth match group.
# See https://docs.python.org/3.7/library/re.html for reference
rules = [
    # Spaces before and after dollar signs
    (r"\$([^\s.;,:!?\)\]}])", r"$ \1", "Put a whitespace before and after each dollar sign."),
    (r"([^\s\(\[{])\$", r"\1 $", "Put a whitespace before and after each dollar sign."),

    # Ill-written operators
    (r"[^\\a-zA-Z]log[^a-zA-Z]", r" \\log", "Use \\log{<argument>} instead of log"),
    (r"[^\\a-zA-Z]exp[^a-zA-Z]", r" \\exp", "Use \\exp{<argument>} instead of exp"),
    (r"[^\\a-zA-Z]lim[^a-zA-Z]", r" \\lim", "Use \\lim instead of lim"),
]


def parseArguments() -> dict:
    parser = argparse.ArgumentParser(description='LaTeX linter',
                                     allow_abbrev=False)

    parser.add_argument('files', nargs='+', type=str)
    parser.add_argument('-d', '--dry-run', action='store_true',
                        help='do not modify files')

    return vars(parser.parse_args())


def processFile(fileName: str, dry_run: bool):
    with open(fileName) as f:
        newFile = str()
        errorCount = 0
        print("Processing file " + fileName + "..."
              + (" (dry run)" if dry_run else ""))

        for line in f:
            fixedLine = line
            errorsFound = []

            for rule in rules:
                oldLine = fixedLine
                fixedLine = re.sub(*rule[:2], fixedLine)

                if oldLine != fixedLine:
                    errorsFound.append(rule[2])

            # If errors found
            if line != fixedLine:
                errorCount += 1
                print("Line:")
                print(Fore.RED + line.strip() + Style.RESET_ALL)
                print("Changed into:")
                print(Fore.GREEN + fixedLine.strip() + Style.RESET_ALL)
                print("Error message(s):")
                for error in errorsFound:
                    print(f"{Fore.RED}{error}{Style.RESET_ALL}")
                print("")

            if not dry_run:
                newFile += fixedLine

    if errorCount:
        print(f"{errorCount} lines containing errors found in {fileName}")
        if not dry_run:
            with open(fileName, 'w') as outFile:
                outFile.write(newFile)
    else:
        print(f"No errors found in {fileName}")


if __name__ == '__main__':
    args = parseArguments()
    for f in args['files']:
        processFile(f, dry_run=args['dry_run'])
